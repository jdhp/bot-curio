=====
Curio
=====

Copyright (c) 2021 Jérémie DECOCK (www.jdhp.org)

* Web site: http://www.jdhp.org/software_en.html#curio
* Online documentation: https://jdhp.gitlab.io/curio
* Examples: https://jdhp.gitlab.io/curio/gallery/

* Notebooks: https://gitlab.com/jdhp/bot-curio-notebooks
* Source code: https://gitlab.com/jdhp/bot-curio
* Issue tracker: https://gitlab.com/jdhp/bot-curio/issues
* Curio on PyPI: https://pypi.org/project/curio
* Curio on Anaconda Cloud: https://anaconda.org/jdhp/curio


Description
===========

Curio robot

Note:

    This project is still in beta stage, so the API is not finalized yet.


Dependencies
============

*  Python >= 3.0

PiCamera
--------

Install on Raspbian::

    sudo apt install python-picamera python3-picamera

Test::

    from picamera import PiCamera
    from time import sleep

    camera = PiCamera()
    camera.start_preview()
    sleep(10)
    camera.stop_preview()

Sources and additional information:

- https://magpi.raspberrypi.org/books/camera-guide (chapter 5)


Pan-Tilt HAT
------------

Install on Raspbian::

    sudo apt install python3-pantilthat

Install for other environments::

    sudo pip3 install pantilthat

Test::

    import pantilthat

    pantilthat.pan(40)
    pantilthat.tilt(40)

Sources and additional information:

- https://shop.pimoroni.com/products/pan-tilt-hat?variant=22408353287
- https://learn.pimoroni.com/tutorial/sandyj/assembling-pan-tilt-hat
- https://github.com/pimoroni/pantilt-hat
- http://docs.pimoroni.com/pantilthat/
- https://github.com/pimoroni/pantilt-hat/blob/master/examples/smooth.py

Tensorflow
----------

Source: https://www.bitsy.ai/3-ways-to-install-tensorflow-on-raspberry-pi/

Official TensorFlow Wheels
++++++++++++++++++++++++++

The TensorFlow team builds and tests binaries for a variety of platform and Python interpreter combination, listed at tensorflow.org/install/pip#package-location.
Unfortunately, the Raspberry Pi wheels drift a bit behind the latest releases (2.3.1 and 2.4.0-rc2 at the time of this writing).
Binaries for aarch64 are not officially supported yet.

Install tensorflow==2.3.0 with pip (requires Python 3.5, Raspberry Pi 3)::

    pip install https://storage.googleapis.com/tensorflow/raspberrypi/tensorflow-2.3.0-cp35-none-linux_armv7l.whl


Community-built Wheels
++++++++++++++++++++++

Built from source at github.com/bitsy-ai/tensorflow-arm-bin. Where possible, it is recommended using the official wheels because they're more thoroughly tested.

Install tensorflow==2.4.0-rc2 with pip (requires Python 3.7, Raspberry Pi 4)::

    pip install https://github.com/bitsy-ai/tensorflow-arm-bin/releases/download/v2.4.0-rc2/tensorflow-2.4.0rc2-cp37-none-linux_armv7l.whl

Install tensorflow==2.4.0-rc2 with pip (requires Python 3.7, Raspberry Pi 4, 64-bit OS)::

    pip install https://github.com/bitsy-ai/tensorflow-arm-bin/releases/download/v2.4.0-rc2/tensorflow-2.4.0rc2-cp37-none-linux_aarch64.whl


Raspberry Pi Deep PanTilt
-------------------------

Install on Raspbian::

    mkdir -p ~/git/rpi-deep-pantilt
    cd ~/git/rpi-deep-pantilt
    python3 -m venv .venv
    source .venv/bin/activate
    python3 -m pip install --upgrade pip
    pip install https://github.com/bitsy-ai/tensorflow-arm-bin/releases/download/v2.4.0-rc2/tensorflow-2.4.0rc2-cp37-none-linux_aarch64.whl
    python3 -m pip install rpi-deep-pantilt
    .venv/bin/rpi-deep-antilt test camera

$ pip install https://github.com/bitsy-ai/tensorflow-arm-bin/releases/download/v2.4.0-rc2/tensorflow-2.4.0rc2-cp37-none-linux_aarch64.whl

- https://towardsdatascience.com/real-time-object-tracking-with-tensorflow-raspberry-pi-and-pan-tilt-hat-2aeaef47e134
- https://github.com/bitsy-ai/rpi-object-tracking


MediaPipe
---------

- From pip: fail (no package for RPi)
- From Docker: fail because of tensorflow 1.14... -> c.f. what have been done for StableBaselines... et c.f. ci dessus
- From source: to test...


.. _install:

Installation
============

Gnu/Linux
---------

You can install, upgrade, uninstall Curio with these commands (in a
terminal)::

    pip install --pre curio
    pip install --upgrade curio
    pip uninstall curio

Or, if you have downloaded the Curio source code::

    python3 setup.py install

.. There's also a package for Debian/Ubuntu::
.. 
..     sudo apt-get install curio

Windows
-------

.. Note:
.. 
..     The following installation procedure has been tested to work with Python
..     3.4 under Windows 7.
..     It should also work with recent Windows systems.

You can install, upgrade, uninstall Curio with these commands (in a
`command prompt`_)::

    py -m pip install --pre curio
    py -m pip install --upgrade curio
    py -m pip uninstall curio

Or, if you have downloaded the Curio source code::

    py setup.py install

MacOSX
-------

.. Note:
.. 
..     The following installation procedure has been tested to work with Python
..     3.5 under MacOSX 10.9 (*Mavericks*).
..     It should also work with recent MacOSX systems.

You can install, upgrade, uninstall Curio with these commands (in a
terminal)::

    pip install --pre curio
    pip install --upgrade curio
    pip uninstall curio

Or, if you have downloaded the Curio source code::

    python3 setup.py install


Documentation
=============

* Online documentation: https://jdhp.gitlab.io/curio
* API documentation: https://jdhp.gitlab.io/curio/api.html


Example usage
=============

TODO


Bug reports
===========

To search for bugs or report them, please use the Curio Bug Tracker at:

    https://gitlab.com/jdhp/bot-curio/issues


License
=======

This project is provided under the terms and conditions of the `MIT License`_.


.. _MIT License: http://opensource.org/licenses/MIT
.. _command prompt: https://en.wikipedia.org/wiki/Cmd.exe
