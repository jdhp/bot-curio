AUTHOR_NAME="Jérémie DECOCK"
AUTHOR_EMAIL="jd.jdhp@gmail.com"
AUTHOR_WEB_SITE="www.jdhp.org"

COPYRIGHT_NOTICE="Copyright (c) 2021 ${AUTHOR_NAME} (${AUTHOR_WEB_SITE})"

PROJECT_INITIAL_DATE="2021-06-20"
PROJECT_SHORT_DESC="Curio robot"

PROJECT_NAME="Curio"                        # TODO e.g. PyAX-12
PYTHON_PACKAGE_NAME="curio"                 # TODO e.g. pyax12 (all in lower case and without special character)

PROJECT_GITHUB_ACCOUNT="jdhp"              # TODO (GitLab)
#PROJECT_GITHUB_ACCOUNT="jeremiedecock"     # TODO (GitHub)
PROJECT_GITHUB_REPOSITORY_NAME="bot-curio"

PROJECT_PYPI_URL="https://pypi.org/project/${PYTHON_PACKAGE_NAME}"
PROJECT_WEB_SITE_URL="http://www.jdhp.org/software_en.html#${PYTHON_PACKAGE_NAME}"

# GITLAB ##############################

PROJECT_GITHUB_URL="https://gitlab.com/${PROJECT_GITHUB_ACCOUNT}/${PROJECT_GITHUB_REPOSITORY_NAME}"
PROJECT_ISSUE_TRACKER_URL="https://gitlab.com/${PROJECT_GITHUB_ACCOUNT}/${PROJECT_GITHUB_REPOSITORY_NAME}/issues"

PROJECT_ONLINE_DOCUMENTATION_URL="https://${PROJECT_GITHUB_ACCOUNT}.gitlab.io/${PYTHON_PACKAGE_NAME}"
PROJECT_ONLINE_API_DOCUMENTATION_URL="https://${PROJECT_GITHUB_ACCOUNT}.gitlab.io/${PYTHON_PACKAGE_NAME}/api.html"

# GITHUB ##############################

#PROJECT_GITHUB_URL="https://github.com/${PROJECT_GITHUB_ACCOUNT}/${PROJECT_GITHUB_REPOSITORY_NAME}"
#PROJECT_ISSUE_TRACKER_URL="https://github.com/${PROJECT_GITHUB_ACCOUNT}/${PROJECT_GITHUB_REPOSITORY_NAME}/issues"

#PROJECT_ONLINE_DOCUMENTATION_URL="http://${PYTHON_PACKAGE_NAME}.readthedocs.org"
#PROJECT_ONLINE_API_DOCUMENTATION_URL="http://${PYTHON_PACKAGE_NAME}.readthedocs.org/en/latest/api.html"
