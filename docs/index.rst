=====
Curio
=====

Curio robot

Note:

    This project is in beta stage, the API may change.

Contents:

.. toctree::
   :maxdepth: 2

   intro
   API <api>
   developer

* Web site: http://www.jdhp.org/software_en.html#curio
* Online documentation: https://jdhp.gitlab.io/curio
* Source code: https://gitlab.com/jdhp/bot-curio
* Issue tracker: https://gitlab.com/jdhp/bot-curio/issues
* curio on PyPI: https://pypi.org/project/curio

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Credits
=======

.. include:: ../AUTHORS
   :literal:

License
=======

.. highlight:: none

.. include:: ../LICENSE
   :literal:

